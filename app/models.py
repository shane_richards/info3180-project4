from . import db  
from sqlalchemy.orm import relationship
from datetime import datetime, timedelta
from uuid import uuid4

# helper table
# items = db.Table('item_wishlist',
#     db.Column('item_id', db.Integer, db.ForeignKey('item.id')),
#     db.Column('wishlist_id', db.Integer, db.ForeignKey('wishlist.id')),
#     db.Column('thumbnail', db.String(500))
# )

class Item(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(80))
    description = db.Column(db.String(500))
    image_url = db.Column(db.String(200))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    wishlist_id = db.Column(db.Integer, db.ForeignKey('wishlist.id'))
    updated_at = db.Column(db.DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow())
    created_at = db.Column(db.DateTime, default=datetime.utcnow())

    def __init__(self, name, description=None):
        self.name = name

        if description is not None:
            self.description = description

    def __repr__(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'thumbnail': self.thumbnail
        }

class Wishlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    description = db.Column(db.String(500))
    updated_at = db.Column(db.DateTime, default=datetime.utcnow())
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    # items = db.relationship('Item', secondary=items,
    #     backref=db.backref('wishlist', lazy='dynamic'))

    def __repr__(self):
        return {
            'name': self.name,
            'description': self.description,
            'updated_at': self.updated_at,
            'created_at': self.created_at,
            'user_id': self.user_id
        }

class AuthToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(200), unique=True)
    expire_at = db.Column(db.DateTime, default=datetime.utcnow())
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, days=60):
        self.token = uuid4().hex
        self.created_at = datetime.utcnow()
        self.expire_at = self.created_at + timedelta(days=days)

    def __repr__(self):
        return {
            'token': self.token,
            'expire_at': self.expire_at,
            'created_at': self.created_at,
            'user_id': self.user_id
        }

class User(db.Model):     
    id = db.Column(db.Integer, primary_key=True)     
    first_name = db.Column(db.String(80))     
    last_name = db.Column(db.String(80)) 
    email = db.Column(db.String(80), unique=True)    
    password = db.Column(db.String(120))
    last_login = db.Column(db.DateTime, default=datetime.utcnow())
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    tokens = db.relationship('AuthToken', backref='User', lazy='dynamic')

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2 support
        except NameError:
            return str(self.id)  # python 3 support

    def __repr__(self):
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email
        }

