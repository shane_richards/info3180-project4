'use strict';

(function(angular){
	angular
		.module('wishlist')
		.controller('ProfileShowCtrl', 
			['$stateParams', '$scope', '$log', '$state', '$http', 'localStorageService', profileShowCtrlFn]);

	function profileShowCtrlFn($stateParams, $scope, $log, $state, $http, localStorageService) {
		var id = localStorageService.get(id);

		$log.log($stateParams.userId + ' id ' + id);

		// if (id == null) {
		// 	$state.go('login');
		// 	return;
		// }

		$http.get('/api/profiles/' + $stateParams.userId)
			.then(function(response) {
				$log.log(response);

				var user = response.data;

				$scope.user = user;
				
			}, function (error) {
				$log.log(error);
			});
	}
	
})(angular);