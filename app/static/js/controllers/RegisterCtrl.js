'use strict';

(function(angular) {
	angular
		.module('wishlist')
		.controller('RegisterCtrl', 
			['$state', '$scope', '$log', '$http', 'localStorageService', registerCtrl]);

	function registerCtrl($state, $scope, $log, $http, localStorageService) {
		if (localStorageService.get('token') != null) {
			$state.go('home');
		}

		$scope.registerUser = function(user) {
			$http.post('/api/register', user)
				.then(function(response) {
					var data = response.data;
					localStorageService.set('token', data.token);
					localStorageService.set('firstName', data.first_name);
					localStorageService.set('id', data.id);
					$scope.$broadcast('user.login');
					$state.go('home');
				}, function(error) {
					$log.log(error);
				});
		};
	}
})(angular);