'use strict';

(function(angular) {
	angular
		.module('wishlist')
		.controller('ItemsNewCtrl', 
			['$scope', '$stateParams', '$log', '$http', itemsNewCtrlFn]);

	function itemsNewCtrlFn($scope, $stateParams, $log, $http) {
		var id = $stateParams.wishlistId;

		$scope.item = {};

		$scope.retreiveImages = function(item) {
			if (item.url == null | item.url.length == 0) {
				$scope.error = "Enter a url";
				return;
			}

			var request = {
				method: 'POST',
				url: 'api/items/get-images',
				data: {
					url: item.url
				}
			};

			$http(request)
				.then(function(res) {
					$scope.images = res.data;
					$log.log($scope.images);
				}, function(err) {
					$log.log(err);
				});
		}
	}
})(angular);