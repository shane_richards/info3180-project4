'use strict';

(function(angular) {
	angular
		.module('wishlist')
		.controller('WishlistNewCtrl', 
			['$scope', '$http', 'localStorageService', '$log', '$state',
			'$stateParams', wishlistNewCtrlFn]);

	function wishlistNewCtrlFn($scope, $http, localStorageService, $log, 
		$state, $stateParams) {
		
		if (localStorageService.get('token') == null) {
			$scope.error = true;
		}

		$scope.createWishlist = function(wishlist){
			var token = localStorageService.get('token');

			if (null == token) return;

			var request = {
				method: 'POST', 
				url: '/api/wishlists',
				headers: {
					'auth-token': token 
				},
				data: {
					name: wishlist.name,
					description: wishlist.description
				}
			};

			$http(request)
				.then(function(response) {
					var data = response.data;
					$log.log(data);
					$state.go('show_wishlist',{wishlistId:data.id});
				}, function(error) {
					$log.log(error);
				});
		}
	}
	
})(angular);