'use strict';

(function(angular) {
	angular
		.module('wishlist')
		.controller('WishlistShowCtrl', 
			['$log', '$http', '$stateParams', wishlistShowCtrlFn]);

	function wishlistShowCtrlFn($log, $http, $stateParams) {
		var id = $stateParams.wishlistId;

		var request = {
			method: 'GET',
			url: '/api/wishlists/' + id
		};

		$http(request)
			.then(function(request) {
				$scope.wishlist = request.data;
				$log.log($scope.wishlist);
			}, function(error) {
				$log.log(error);
			});
	}
	
})(angular);