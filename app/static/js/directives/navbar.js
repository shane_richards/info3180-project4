'use strict';

(function(angular){
	angular
		.module('wishlist')
		.directive('navbar',
			['$state', '$log', 'localStorageService', navbarFn]);

	function navbarFn($state, $log, localStorageService) {
		return {
			scope: {},
			restrict: 'AE',
			replace: 'true',
			templateUrl: 'partials/navigation/navbar.html',
			link: function(scope, elem, attr) {
				if (localStorageService.get('token') != null)
					updateUser()
				else
					scope.user = null;

				scope.$on('user.login', function() {
					updateUser();
				});

				scope.$on('user.logout', function() {
					scope.user = null;
				});

				function updateUser() {
					var token = localStorageService.get('token');

					if (token == null) return;

					scope.user = {
						firstName: localStorageService.get('firstName'),
						lastName: localStorageService.get('lastName'),
						token: token
					};
				}
			}
		}
	}
})(angular);