"""
Flask Documentation:     http://flask.pocoo.org/docs/
Jinja2 Documentation:    http://jinja.pocoo.org/2/documentation/
Werkzeug Documentation:  http://werkzeug.pocoo.org/documentation/

This file creates your application.
"""

from flask import render_template, request, redirect, url_for,jsonify,g,session

from werkzeug.datastructures import MultiDict
from app.models import User, AuthToken, Wishlist
from app.forms import LoginForm, RegisterForm

from flask.ext.login import login_user, logout_user, current_user, login_required
from sqlalchemy.exc import IntegrityError
from app import app, db, lm, oid

from requests import get
from bs4 import BeautifulSoup
import json

@app.before_request
def before_request():
    g.user = current_user

@app.route('/')
@app.route('/home')
def home():
    """Render website's home page."""
    return app.send_static_file('index.html')

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def assets(path):
    """Respond with static files"""
    return app.send_static_file(path)

@app.route('/api/register', methods=['POST'])
def register():
    data = MultiDict(mapping=request.json)
    form = RegisterForm(data, csrf_enabled=False)

    # if not form.validate():
    #     return jsonify({'error': 'please correct the form'})

    if data.get('password') != data.get('confirmation'):
        return jsonify({'error': 'password and confirmation do not match'})

    auth = AuthToken()

    new_user = User(first_name=data.get('firstName'),
                    last_name=data.get('lastName'),
                    email=data.get('email'),
                    password=data.get('password'))

    new_user.tokens.append(auth)

    try:
        db.session.add(new_user)
        db.session.add(auth)
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': 'email already taken'})

    return jsonify({
        'id': new_user.id,
        'first_name': new_user.first_name,
        'last_name': new_user.last_name,
        'token': auth.token,
        'last_login': new_user.last_login
    })

    return jsonify(response)

@app.route('/api/login', methods=['POST'])
def login():
    data = MultiDict(mapping=request.json)
    form = LoginForm(data, csrf_enabled=False)

    if not form.validate():
        return jsonify({'error': 'invalid email or password'})
    
    email = data.get('email')
    password = data.get('password')

    user = db.session.query(User).filter_by(email=data['email']).first()

    if not user:
        return jsonify({'error': 'email not found'})

    if not user.password == data.get('password'):
        return jsonify({'error': 'invalid credentials'})

    auth = user.tokens[0].__repr__()

    return jsonify({
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'token': auth.get('token'),
        'last_login': user.last_login
    })

@app.route('/api/logout')
def logout():
    return jsonify({
        'message': 'logout successful'
    })

@app.route('/api/profiles/:id', methods=['GET'])
def user_details(id):
    profile = User.query.get(id)
    return jsonify(profile.__repr__())

@app.route('/api/wishlists', methods=['POST'])
def wishlist_save():
    data = MultiDict(mapping=request.json)

    if not 'auth-token' in request.headers:
        return jsonify({'error': 'token missing'})

    token = db.session.query(AuthToken).filter_by(token=request.headers['auth-token']).first()
    user = db.session.query(User).filter_by(id=token.user_id).first()

    wishlist = Wishlist(name=data.get('name'),
        description=data.get('description'),
        user_id=user.id)

    try:
        db.session.add(wishlist)
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': 'email already taken'})

    response = wishlist.__repr__()

    response.update({
        'id': wishlist.id,
        'user_id': wishlist.user_id
    })

    return jsonify(response)

@app.route('/api/wishlists/<int:id>', methods=['GET'])
def wishlist_details(id):
    wishlist = Wishlist.query.get(id)
    return jsonify(wishlist.__repr__())

@app.route('/api/items/get-images', methods=['POST'])
def retrieve_images():
    data = request.get_json()
    url = data['url']
    r = get(url)
    data = r.text
    soup = BeautifulSoup(data, "html.parser")

    images = []

    #Images from Amazon
    spans = []
    result_set = soup.find_all('img')
    for span in result_set:
        if not 'gif' in span.get('src') and not 'png' in span.get('src') and not 'sprite' in span.get('src'):
            images.append(span.get('src'))

    if 'amazon' in request.get_json()['url']:
        title = soup.find('span' , {'id':'productTitle'})
    elif 'newegg' in request.get_json()['url']:
        title = soup.find('span' , {'id':'grpDescrip_0'})
    elif 'ebay' in request.get_json()['url']:
        title = soup.find('h1' , {'id':'itemTitle'})
        print len(title)

    if title:
        title = title.getText()
    else:
        title = ''
    images = map(lambda x: {'url' : x} ,images)
    result = {'images': images ,'title' : title}
    return json.dumps(result)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port="8888")
